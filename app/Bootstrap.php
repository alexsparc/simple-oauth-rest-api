<?php

namespace App;

use App\Component\OAuth\Middleware\ResourceServerMiddleware;
use App\Component\OAuth\Repository\AccessTokenRepository;
use App\Component\OAuth\Repository\ClientRepository;
use App\Component\OAuth\Repository\ScopeRepository;
use App\Component\Validator\Validator;
use App\Contract\Service\HostawayServiceContract;
use App\Middleware\ApiLoggerMiddleware;
use App\Service\Hostaway\HostawayService;
use ErrorException;
use Exception;
use GuzzleHttp\Psr7\ServerRequest;
use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Router;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use League\OAuth2\Server\ResourceServer;
use Memcached;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class Bootstrap
{
    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var array
     */
    private $config;

    /**
     * Bootstrap constructor.
     */
    public function __construct()
    {
        $this->rootDir = dirname(__DIR__);
        $this->config = include($this->rootDir . '/config/app.php');
    }

    /**
     * @return App
     */
    public function buildApp()
    {
        $container = new Container();

        /** Fill container with new bindings */
        $this->registerComponents($container);

        /** Build router */
        $router = $this->bootRouter($container);

        $app = new App($container, $router);

        error_reporting(-1);
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleException']);
        register_shutdown_function([$this, 'handleShutdown']);

        return $app;
    }

    /**
     * Filling container by instances and bindings
     * @param Container $container
     */
    protected function registerComponents(Container $container)
    {
        /** Register Logger */
        $logger = new Logger($this->config['logger']['channel']);
        $logger->pushHandler(new RotatingFileHandler(
            $this->rootDir . '/' . $this->config['logger']['output'], 10, Logger::INFO, true, 755));
        $container->instance(Logger::class, $logger);

        /** Register ORM */
        $manager = new Manager;
        $manager->addConnection($this->config['db']);
        $container->instance(Manager::class, $manager);

        /** Register Memcached */
        $cacheConfig = $this->config['cache'];
        $cache = new Memcached();
        $cache->addServer($cacheConfig['host'], $cacheConfig['port'], $cacheConfig['weight']);
        $container->instance(Memcached::class, $cache);

        /** Register Request */
        $container->instance(Request::class, Request::capture());
        $container->instance(ServerRequest::class, ServerRequest::fromGlobals());

        /** Register Hostaway API Service */
        $container->bind(HostawayServiceContract::class, HostawayService::class);

        /** Request Validator */
        $container->instance(Validator::class, new Validator());

        /** OAuth */
        $oauthConfig = $this->config['oauth'];

        $container->bind(ClientRepositoryInterface::class, ClientRepository::class);
        $container->bind(ScopeRepositoryInterface::class, ScopeRepository::class);
        $container->bind(AccessTokenRepositoryInterface::class, AccessTokenRepository::class);

        $server = new AuthorizationServer(
            $container->make(ClientRepositoryInterface::class),
            $container->make(AccessTokenRepositoryInterface::class),
            $container->make(ScopeRepositoryInterface::class),
            new CryptKey($this->rootDir . '/' . $oauthConfig['key'], $oauthConfig['pwd']),
            $oauthConfig['enc.key']
        );
        $server->enableGrantType(new ClientCredentialsGrant());
        $container->instance(AuthorizationServer::class, $server);

        $container->instance(ResourceServer::class, new ResourceServer(
            $container->make(AccessTokenRepositoryInterface::class),
            $this->rootDir . '/' . $this->config['oauth']['key.pub']
        ));
    }

    /**
     * Route management
     * @param Container $container
     * @return Router
     */
    protected function bootRouter(Container $container)
    {
        $events = new Dispatcher($container);
        $router = new Router($events, $container);

        /** Define Middleware */
        $router->middlewareGroup('api', [
            ApiLoggerMiddleware::class
        ]);

        $router->middlewareGroup('oauth', [
            ResourceServerMiddleware::class
        ]);

        /** Define Routes */
        $router->group(['namespace' => 'App\Controller', 'middleware' => ['api']], function(Router $router) {

            $router->group(['middleware' => ['oauth'], 'prefix' => 'api/contacts'], function (Router $router) {
                $router->post('create', ['uses' => 'ContactsController@create']);
                $router->put('{id}', ['uses' => 'ContactsController@update'])->where('id', '\d+');
                $router->get('{id}', ['uses' => 'ContactsController@get'])->where('id', '\d+');
                $router->delete('{id}', ['uses' => 'ContactsController@delete'])->where('id', '\d+');
                $router->post('search', ['uses' => 'ContactsController@search']);
            });

            $router->group(['prefix' => 'oauth'], function(Router $router) {
                $router->post('issue-token', 'OAuthController@issueToken');
            });

            /** Route mismatch :/ */
            $router->any('{any}', ['uses' => 'TeapotController@ooops'])->where('any', '(.*)?');
        });


        return $router;
    }

    /**
     * Convert PHP errors to ErrorException instances.
     *
     * @param  int  $level
     * @param  string  $message
     * @param  string  $file
     * @param  int  $line
     * @param  array  $context
     * @return void
     *
     * @throws \ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        if (error_reporting() & $level) {
            throw new ErrorException($message, 0, $level, $file, $line);
        }
    }

    /**
     * Handle an uncaught exception from the application.
     *
     * Note: Most exceptions can be handled via the try / catch block in
     * the HTTP and Console kernels. But, fatal error exceptions must
     * be handled differently since they are not normal exceptions.
     *
     * @param  \Throwable  $e
     * @return void
     */
    public function handleException($e)
    {
        if (! $e instanceof Exception) {
            $e = new FatalThrowableError($e);
        }

        App::getContainer()->make(Logger::class)->error($e->getMessage(), [
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => $e->getTraceAsString(),
        ]);

        (new Response('I\'m dead! My creators will get punished soon!', Response::HTTP_INTERNAL_SERVER_ERROR))->send();
    }

    /**
     * Handle the PHP shutdown event.
     *
     * @return void
     */
    public function handleShutdown()
    {
        if (! is_null($error = error_get_last()) && $this->isFatal($error['type'])) {
            $this->handleException($this->fatalExceptionFromError($error, 0));
        }
    }

    /**
     * Determine if the error type is fatal.
     *
     * @param  int  $type
     * @return bool
     */
    protected function isFatal($type)
    {
        return in_array($type, [E_COMPILE_ERROR, E_CORE_ERROR, E_ERROR, E_PARSE]);
    }

    /**
     * Create a new fatal exception instance from an error array.
     *
     * @param  array  $error
     * @param  int|null  $traceOffset
     * @return \Symfony\Component\Debug\Exception\FatalErrorException
     */
    protected function fatalExceptionFromError(array $error, $traceOffset = null)
    {
        return new FatalErrorException(
            $error['message'], $error['type'], 0, $error['file'], $error['line'], $traceOffset
        );
    }

}