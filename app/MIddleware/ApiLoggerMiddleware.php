<?php

namespace App\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Monolog\Logger;

class ApiLoggerMiddleware
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * ApiLoggerMiddleware constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        /**
         * Log request data
         */
        $this->logger->info('Received request', [
            'headers' => $request->headers->all(),
            'body' => stream_get_contents($request->getContent(true))
        ]);

        /**
         * Pass request further
         * @var Response $response
         */
        $response = $next($request);

        /**
         * Log response
         */
        $this->logger->info('Sending response', [
            'headers' => $response->headers->all(),
            'body' => $response->getContent()
        ]);

        return $response;
    }

}