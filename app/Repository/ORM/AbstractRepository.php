<?php

namespace App\Repository\ORM;

use App\App;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Query\Builder;

abstract class AbstractRepository
{
    /**
     * @return string
     */
    abstract protected function getTable(): string;

    /**
     * @return Builder
     */
    public function getQueryBuilder(): Builder
    {
        return App::getContainer()->make(Manager::class)->getConnection()->table($this->getTable());
    }

}