<?php

namespace App\Repository\ORM;

trait CrudOperations
{
    /**
     * @param array $entity
     * @return bool
     */
    public function create(array $entity): bool
    {
        return $this->getQueryBuilder()->insert($entity);
    }

    /**
     * @param array $entity
     * @return bool
     */
    public function update(array $entity): bool
    {
        return $this->getQueryBuilder()->where('id', $entity['id'])->update($entity);
    }

    /**
     * @param int|string $id
     * @return null|object|\StdClass
     */
    public function get($id): ?\StdClass
    {
        return $this->getQueryBuilder()->find($id);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        return (bool)$this->getQueryBuilder()->where('id', $id)->delete();
    }

}