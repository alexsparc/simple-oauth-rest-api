<?php

namespace App\Repository\ORM;

class OAuthAccessTokenRepository extends AbstractRepository
{
    use CrudOperations, SearchOperations;

    /**
     * @return string
     */
    protected function getTable(): string
    {
        return 'oauth_access_tokens';
    }

    /**
     * @return array
     */
    protected function getSearchableFields(): array
    {
        return ['id', 'name', 'email'];
    }

    /**
     * @param string $tokenId
     * @return int
     */
    public function revokeAccessToken($tokenId): int
    {
        return $this->getQueryBuilder()->where('id', $tokenId)->update(['revoked' => 1]);
    }

    /**
     * @param string $tokenId
     * @return bool
     */
    public function isAccessTokenRevoked($tokenId): bool
    {
        $token = $this->get($tokenId);
        return !$token || (bool)$token->revoked;
    }

}