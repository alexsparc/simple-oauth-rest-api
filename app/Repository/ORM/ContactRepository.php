<?php

namespace App\Repository\ORM;

use App\Contract\ORM\CrudRepositoryContract;
use App\Contract\ORM\SearchRepositoryContract;

class ContactRepository extends AbstractRepository implements CrudRepositoryContract, SearchRepositoryContract
{
    use CrudOperations, SearchOperations;

    /**
     * @return string
     */
    protected function getTable(): string
    {
        return 'contacts';
    }

    /**
     * @return array
     */
    protected function getSearchableFields(): array
    {
        return ['first_name', 'last_name', 'phone', 'country_code', 'timezone_name'];
    }

}