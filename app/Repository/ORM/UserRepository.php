<?php

namespace App\Repository\ORM;

use App\Contract\ORM\CrudRepositoryContract;
use App\Contract\ORM\SearchRepositoryContract;

class UserRepository extends AbstractRepository implements CrudRepositoryContract, SearchRepositoryContract
{
    use CrudOperations, SearchOperations;

    /**
     * @return string
     */
    protected function getTable(): string
    {
        return 'users';
    }

    /**
     * @return array
     */
    protected function getSearchableFields(): array
    {
        return ['id', 'name', 'email'];
    }

}