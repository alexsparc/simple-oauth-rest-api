<?php

namespace App\Repository\ORM;

use App\App;
use App\Component\Paginator;

trait SearchOperations
{
    /**
     * @param string $keyword
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function search(string $keyword, int $page, int $pageSize): array
    {
        /**
         * Prepare query
         */
        $query = $this->getQueryBuilder();
        foreach ($this->getSearchableFields() as $key) {
            $query->orWhere($key, 'like', '%' . $keyword . '%');
        }

        /** @var Paginator $paginator */
        $paginator = App::getContainer()->make(Paginator::class);
        $pagination = $paginator->paginate($query, $page, $pageSize);

        return [
            'keyword' => $keyword,
            'pagination' => $pagination,
            'items' => $query->offset($pagination['currentOffset'])->limit($pagination['pageSize'])->get(),
        ];
    }

}