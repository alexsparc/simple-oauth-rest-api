<?php

namespace App\Repository\ORM;

use App\Contract\ORM\CrudRepositoryContract;

class OAuthClientRepository extends AbstractRepository implements CrudRepositoryContract
{
    use CrudOperations;

    /**
     * @return string
     */
    protected function getTable(): string
    {
        return 'oauth_clients';
    }

    /**
     * @param string $id
     * @return \stdClass|object|null
     */
    public function findActive(string $id): ?\stdClass
    {
        return $this->getQueryBuilder()->where('id', $id)->where('revoked', 0)->first();
    }

}