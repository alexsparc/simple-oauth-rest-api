<?php

namespace App;

use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;

class App
{
    /**
     * @var App
     */
    private static $instance;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Router
     */
    private $router;

    /**
     * @param Container $container
     * @param Router $router
     */
    public function __construct(Container $container, Router $router)
    {
        $this->container = $container;
        $this->router = $router;

        /** Make it Global! */
        static::$instance = $this;
    }

    /**
     * Dispatch request
     */
    public function run()
    {
        /** @var Request $request */
        $request = $this->container->make(Request::class);

        $this->router->dispatch($request)->send();
    }

    /**
     * @return Container
     */
    public static function getContainer(): Container
    {
        return static::$instance->container;
    }

}
