<?php

namespace App\Component\Validator;

use App\Contract\Component\ValidatorContract;

class RegexValidator extends AbstractValidator
{
    /**
     * @inheritdoc
     */
    public function validate(array $data, string $field, array $options = []): ValidatorContract
    {
        /**
         * Ignore check if field is empty and not required
         */
        $this->result = empty($data[$field]) ? true : preg_match($options[0], $data[$field]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getMessage(): string
    {
        return 'Regex check ' . ($this->getResult() ? 'succeeded' : 'failed');
    }

}