<?php

namespace App\Component\Validator;

use App\Contract\Component\ValidatorContract;
use App\Contract\Service\HostawayServiceContract;

class CountryCodeValidator extends AbstractValidator
{
    /**
     * @var HostawayServiceContract
     */
    private $hostawayService;

    /**
     * @param HostawayServiceContract $hostawayService
     */
    public function __construct(HostawayServiceContract $hostawayService)
    {
        $this->hostawayService = $hostawayService;
    }

    /**
     * @inheritdoc
     */
    public function validate(array $data, string $field, array $options = []): ValidatorContract
    {
        $this->result = $this->hostawayService->getCountries()->has($data[$field]);;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getMessage(): string
    {
        return 'Country code check ' . ($this->getResult() ? 'succeeded' : 'failed');
    }

}