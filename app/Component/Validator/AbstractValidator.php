<?php

namespace App\Component\Validator;

use App\Contract\Component\ValidatorContract;

abstract class AbstractValidator implements ValidatorContract
{
    /**
     * @var bool
     */
    protected $result;

    /**
     * @return bool
     */
    public function getResult(): bool
    {
        return $this->result;
    }

}