<?php

namespace App\Component\Validator;

use App\Contract\Component\ValidatorContract;
use App\Contract\Service\HostawayServiceContract;

class TimezoneNameValidator extends AbstractValidator
{
    /**
     * @var HostawayServiceContract
     */
    private $hostawayService;

    /**
     * @param HostawayServiceContract $hostawayService
     */
    public function __construct(HostawayServiceContract $hostawayService)
    {
        $this->hostawayService = $hostawayService;
    }

    /**
     * @inheritdoc
     */
    public function validate(array $data, string $field, array $options = []): ValidatorContract
    {
        $this->result = $this->hostawayService->getTimeZones()->has($data[$field]);;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getMessage(): string
    {
        return 'Timezone name check ' . ($this->getResult() ? 'succeeded' : 'failed');
    }

}