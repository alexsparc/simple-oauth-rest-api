<?php

namespace App\Component\Validator;

use App\App;
use App\Contract\Component\ValidatorContract;

class Validator
{
    protected $validators = [
        'required' => RequiredValidator::class,
        'regex' => RegexValidator::class,
        'countryCode' => CountryCodeValidator::class,
        'timezoneName' => TimezoneNameValidator::class,
    ];

    /**
     * @param array $data
     * @param array $validationConfig
     * @return array
     */
    public function validate(array $data, array $validationConfig): array
    {
        $result = [];

        /** Walk through validation config */
        foreach ($validationConfig as $fieldName => $rules) {
            $validationResult = [];
            foreach ($rules as $rule) {
                /**
                 * @var string $validator
                 * @var array $options
                 */
                extract($this->deserializeRule($rule));

                /** Skip invalid rule */
                if (!array_key_exists($validator, $this->validators)) {
                    continue;
                }

                /** @var ValidatorContract $v */
                $v = App::getContainer()->make($this->validators[$validator]);

                $v->validate($data, $fieldName, $options);
                if (!$v->getResult()) {
                    array_push($validationResult, $v->getMessage());
                }
            }

            if (!empty($validationResult)) {
                $result[$fieldName] = $validationResult;
            }
        }

        return $result;
    }

    /**
     * @param string $rule
     * @return array
     */
    public function deserializeRule(string $rule): array
    {
        preg_match('#^([a-z]+):(.*)$#', $rule, $matches);

        return empty($matches) ?
            ['validator' => $rule, 'options' => []] :
            ['validator' => $matches[1], 'options' => [$matches[2]]];
    }

}