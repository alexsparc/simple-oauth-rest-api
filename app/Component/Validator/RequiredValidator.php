<?php

namespace App\Component\Validator;

use App\Contract\Component\ValidatorContract;

class RequiredValidator extends AbstractValidator
{
    /**
     * @inheritdoc
     */
    public function validate(array $data, string $field, array $options = []): ValidatorContract
    {
        $this->result = isset($data[$field]) && !empty($data[$field]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getMessage(): string
    {
        return 'Requirement check ' . ($this->getResult() ? 'succeeded' : 'failed');
    }

}