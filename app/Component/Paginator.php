<?php

namespace App\Component;

use Illuminate\Database\Query\Builder;

class Paginator
{
    /**
     * @var int
     */
    private $firstPage = 1;

    /**
     * @var int
     */
    private $minPageSize = 1;

    /**
     * @var int
     */
    private $maxPageSize = 20;

    /**
     * @param Builder $builder
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function paginate(Builder $builder, int $page, int $pageSize)
    {
        /**
         * Get count for current query
         */
        $totalItems = $builder->count('id');

        /**
         * Min page num from 1 to 20
         */
        $page = max($this->firstPage, $page);

        /**
         * Limit page size from 1 to 20
         */
        $pageSize = min($this->maxPageSize, max($this->minPageSize, $pageSize));

        /**
         * Count number of pages
         */
        $totalPages = intval($totalItems / $pageSize);

        /**
         * Here is Cap speaking! :D
         * If number of found items greater than zero and less than size of page then we will increment number of pages
         */
        if ($totalItems > 0 && $totalItems < $pageSize) {
            $totalPages++;
        }

        return [
            'currentPage' => $page,
            'currentOffset' => ($page - 1) * $pageSize,
            'pageSize' => $pageSize,
            'totalItems' => $totalItems,
            'totalPages' => $totalPages,
        ];
    }
}