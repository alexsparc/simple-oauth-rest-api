<?php

namespace App\Component\OAuth\Middleware;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Illuminate\Http\Request;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;

class ResourceServerMiddleware
{
    /**
     * @var ResourceServer
     */
    private $resourceServer;

    /**
     * @var ServerRequest
     */
    private $request;

    /**
     * @param ResourceServer $resourceServer
     * @param ServerRequest $request
     */
    public function __construct(ResourceServer $resourceServer, ServerRequest $request)
    {
        $this->resourceServer = $resourceServer;
        $this->request = $request;
    }

    /**
     * @param Request $request
     * @param \Closure $next
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function handle(Request $request, \Closure $next)
    {
        $psr7Response = null;

        try {
            $this->resourceServer->validateAuthenticatedRequest($this->request);
        } catch (OAuthServerException $exception) {
            $psr7Response = $exception->generateHttpResponse(new Response());
        } catch (\Exception $exception) {
            $e = new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 500);
            $psr7Response = $e->generateHttpResponse(new Response());
        }

        /** If something went wrong */
        if ($psr7Response) {
            return (new HttpFoundationFactory())->createResponse($psr7Response);
        }

        return $next($request);
    }

}