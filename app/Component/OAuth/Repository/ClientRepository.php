<?php

namespace App\Component\OAuth\Repository;

use App\Component\OAuth\Entity\Client;
use App\Repository\ORM\OAuthClientRepository;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

class ClientRepository implements ClientRepositoryInterface
{
    /**
     * @var OAuthClientRepository
     */
    protected $oauthClientRepository;

    /**
     * @param OAuthClientRepository $oauthClientRepository
     */
    public function __construct(OAuthClientRepository $oauthClientRepository)
    {
        $this->oauthClientRepository = $oauthClientRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getClientEntity($clientIdentifier, $grantType = null, $clientSecret = null,
                                    $mustValidateSecret = true)
    {
        $record = $this->oauthClientRepository->findActive($clientIdentifier);

        if (!$record) {
            return null;
        }

        if ($mustValidateSecret && !hash_equals($record->secret, (string)$clientSecret)) {
            return null;
        }

        return new Client($clientIdentifier, $record->name, $record->redirect);
    }

}
