<?php

namespace App\Component\OAuth\Repository;

use App\Component\OAuth\Entity\AccessToken;
use App\Repository\ORM\OAuthAccessTokenRepository;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;

class AccessTokenRepository implements AccessTokenRepositoryInterface
{

    /**
     * @var OAuthAccessTokenRepository
     */
    private $oauthAccessTokenRepository;

    /**
     * @param OAuthAccessTokenRepository $oauthAccessTokenRepository
     */
    public function __construct(OAuthAccessTokenRepository $oauthAccessTokenRepository)
    {
        $this->oauthAccessTokenRepository = $oauthAccessTokenRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null)
    {
        return new AccessToken($userIdentifier, $scopes);
    }

    /**
     * {@inheritdoc}
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity)
    {
        $this->oauthAccessTokenRepository->create([
            'id' => $accessTokenEntity->getIdentifier(),
            'user_id' => $accessTokenEntity->getUserIdentifier(),
            'client_id' => $accessTokenEntity->getClient()->getIdentifier(),
            'scopes' => implode(' ', $this->scopesToArray($accessTokenEntity->getScopes())),
            'revoked' => false,
            'expires_at' => $accessTokenEntity->getExpiryDateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function revokeAccessToken($tokenId)
    {
        $this->oauthAccessTokenRepository->revokeAccessToken($tokenId);
    }

    /**
     * {@inheritdoc}
     */
    public function isAccessTokenRevoked($tokenId)
    {
        return $this->oauthAccessTokenRepository->isAccessTokenRevoked($tokenId);
    }

    /**
     * @param ScopeEntityInterface[] $scopes
     * @return array
     */
    public function scopesToArray(array $scopes): array
    {
        return array_map(function(ScopeEntityInterface $scope) {
            return $scope->getIdentifier();
        }, $scopes);
    }
}