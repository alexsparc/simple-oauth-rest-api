<?php

namespace App\Service\Hostaway;

use App\Contract\Service\HostawayServiceContract;
use Illuminate\Support\Collection;
use Memcached;

class HostawayService implements HostawayServiceContract
{
    /**
     * @var Memcached
     */
    private $cache;

    /**
     * @var HostawayApiClient
     */
    private $apiClient;

    /**
     * @param Memcached $cache
     */
    public function __construct(Memcached $cache)
    {
        $this->cache = $cache;
        $this->apiClient = new HostawayApiClient();
    }

    /**
     * @return Collection
     */
    public function getCountries(): Collection
    {
        return $this->get('hostaway_api_countries', function() {
            return $this->apiClient->getCountries();
        });
    }

    /**
     * @return Collection
     */
    public function getTimeZones(): Collection
    {
        return $this->get('hostaway_api_timezones', function() {
            return $this->apiClient->getTimeZones();
        });
    }

    /**
     * @param string $key
     * @param callable $resolver
     * @return Collection
     */
    private function get(string $key, callable $resolver): Collection
    {
        if (!in_array($key, $this->cache->getAllKeys())) {
            $this->cache->set($key, $resolver(), 3600);
        }

        return collect($this->cache->get($key));
    }

}