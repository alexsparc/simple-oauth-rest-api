<?php

namespace App\Service\Hostaway;

use GuzzleHttp\Client;
use Illuminate\Http\Response;

class HostawayApiClient
{
    /**
     * @var string
     */
    private $url = 'https://api.hostaway.com';

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => $this->url]);
    }

    /**
     * @return array
     */
    public function getCountries(): array
    {
        return $this->get('countries');
    }

    /**
     * @return array
     */
    public function getTimeZones(): array
    {
        return $this->get('timezones');
    }

    /**
     * @param string $uri
     * @return array
     */
    private function get(string $uri): array
    {
        $response = $this->client->get('/' . $uri);

        return $response->getStatusCode() == Response::HTTP_OK ?
            json_decode($response->getBody()->getContents(), 1)['result'] :
            [];
    }

}