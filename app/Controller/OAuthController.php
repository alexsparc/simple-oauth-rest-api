<?php

namespace App\Controller;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;

class OAuthController
{

    /**
     * @var AuthorizationServer
     */
    private $server;

    /**
     * @param AuthorizationServer $server
     */
    public function __construct(AuthorizationServer $server)
    {
        $this->server = $server;
    }

    /**
     * @param ServerRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function issueToken(ServerRequest $request)
    {
        $psr7Response = new Response();

        try {
            $psr7Response = $this->server->respondToAccessTokenRequest($request, $psr7Response);
        } catch (OAuthServerException $exception) {
            $psr7Response = $exception->generateHttpResponse($psr7Response);
        } catch (\Exception $exception) {
            $e = new OAuthServerException($exception->getMessage(), 0, 'unknown_error', 400);
            $psr7Response = $e->generateHttpResponse($psr7Response);
        }

        return (new HttpFoundationFactory())->createResponse($psr7Response);
    }

}