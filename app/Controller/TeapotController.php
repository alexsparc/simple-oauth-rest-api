<?php

namespace App\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class TeapotController
{
    /**
     * @return JsonResponse
     */
    public function ooops(): JsonResponse
    {
        return new JsonResponse(null, Response::HTTP_I_AM_A_TEAPOT);
    }

}