<?php

namespace App\Controller;

use App\Component\Validator\Validator;
use App\Contract\Service\HostawayServiceContract;
use App\Repository\ORM\ContactRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ContactsController
{
    /**
     * @var ContactRepository
     */
    protected $contactRepository;

    /**
     * @var HostawayServiceContract
     */
    protected $hostawayService;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * @param ContactRepository $contactRepository
     * @param HostawayServiceContract $hostawayService
     * @param Validator $validator
     */
    public function __construct(ContactRepository $contactRepository, HostawayServiceContract $hostawayService,
                                Validator $validator)
    {
        $this->contactRepository = $contactRepository;
        $this->hostawayService = $hostawayService;
        $this->validator = $validator;
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function get(int $id): JsonResponse
    {
        return new JsonResponse($this->contactRepository->get($id));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $data = $request->only(['first_name', 'last_name', 'phone', 'country_code', 'timezone_name']);

        $errors = $this->validator->validate($data, [
            'first_name' => ['required', 'regex:#^[a-zA-Z\'\-]+$#u'],
            'last_name' => ['regex:#^[a-zA-Z\'\-]+$#u'],
            'phone' => ['required', 'regex:#^[\d\+\- ()]+$#u'],
            'country_code' => ['required', 'countryCode'],
            'timezone_name' => ['required', 'timezoneName']
        ]);

        if (!empty($errors)) {
            return new JsonResponse(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        $this->contactRepository->create($data);
        return new JsonResponse();
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        $data = $request->only(['id', 'first_name', 'last_name', 'phone', 'country_code', 'timezone_name']);

        $errors = $this->validator->validate($data, [
            'id' => ['required', 'regex:#^\d+$#'],
            'first_name' => ['required', 'regex:#^[a-zA-Z\'\-]+$#u'],
            'last_name' => ['regex:#^[a-zA-Z\'\-]+$#u'],
            'phone' => ['required', 'regex:#^[\d\+\- ()]+$#'],
            'country_code' => ['required', 'countryCode'],
            'timezone_name' => ['required', 'timezoneName']
        ]);

        if (!empty($errors)) {
            return new JsonResponse(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        $this->contactRepository->update($data);
        return new JsonResponse();
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        return new JsonResponse($this->contactRepository->delete($id));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function search(Request $request): JsonResponse
    {
        $data = [
            'keyword' => $request->get('keyword', ''),
            'page' => $request->get('page', 1),
            'pageSize' => $request->get('pageSize', 20),
        ];

        $errors = $this->validator->validate($data, [
            'keyword' => ['required', 'regex:#^[\w\+\-: ]+$#u'],
            'page' => ['regex:#^\d+$#'],
            'pageSize' => ['regex:#^\d+$#'],
        ]);

        if (!empty($errors)) {
            return new JsonResponse(['errors' => $errors], Response::HTTP_BAD_REQUEST);
        }

        /**
         * @var string $keyword
         * @var int $page
         * @var int $pageSize
         */
        extract($data);
        return new JsonResponse($this->contactRepository->search($keyword, $page, $pageSize));
    }

}