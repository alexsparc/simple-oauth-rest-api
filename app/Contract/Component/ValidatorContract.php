<?php

namespace App\Contract\Component;

interface ValidatorContract
{
    /**
     * @param array $data
     * @param string $field
     * @param array $options
     * @return self
     */
    public function validate(array $data, string $field, array $options = []): self;

    /**
     * @return bool
     */
    public function getResult(): bool;

    /**
     * @return string
     */
    public function getMessage(): string;
}