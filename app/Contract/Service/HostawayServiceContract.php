<?php

namespace App\Contract\Service;

use Illuminate\Support\Collection;

interface HostawayServiceContract
{
    /**
     * @return Collection
     */
    public function getCountries(): Collection;

    /**
     * @return Collection
     */
    public function getTimeZones(): Collection;

}