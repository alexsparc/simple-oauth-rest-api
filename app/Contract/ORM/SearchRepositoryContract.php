<?php

namespace App\Contract\ORM;

interface SearchRepositoryContract
{
    /**
     * @param string $keyword
     * @param int $page
     * @param int $pageSize
     * @return array
     */
    public function search(string $keyword, int $page, int $pageSize): array;
}