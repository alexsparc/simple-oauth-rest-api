<?php

namespace App\Contract\ORM;

interface CrudRepositoryContract
{
    /**
     * @param array $contact
     * @return bool
     */
    public function create(array $contact): bool;

    /**
     * @param array $contact
     * @return bool
     */
    public function update(array $contact): bool;

    /**
     * @param int|string $id
     * @return null|object|\StdClass
     */
    public function get($id): ?\StdClass;

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool;
}