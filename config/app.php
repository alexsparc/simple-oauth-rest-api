<?php

return [
    'logger' => [
        'channel' => 'MAIN',
        'output' => 'storage/logs/app.log',
    ],
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'hostaway',
        'username' => 'hostaway',
        'password' => 'secret',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ],
    'cache' => [
        'host' => '127.0.0.1',
        'port' => 11211,
        'weight' => 100,
    ],
    'oauth' => [
        'key' => '../.ssl/hostaway',
        'key.pub' => '../.ssl/hostaway.pub',
        'pwd' => 'pass:hostaway',
        'enc.key' => 'omSu/kGCzrUE3oqf5v7+tNYro48kQZ2+rMj74Mborao='
    ]
];