<?php

ini_set('display_errors', 1);

require '../vendor/autoload.php';

(new \App\Bootstrap)->buildApp()->run();
