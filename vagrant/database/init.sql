CREATE DATABASE IF NOT EXISTS `hostaway`
  DEFAULT CHARACTER SET utf8mb4
  DEFAULT COLLATE utf8mb4_unicode_ci;
USE `hostaway`;

/**
 * Contacts
 */

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id`            INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name`    VARCHAR(32)      NOT NULL,
  `last_name`     VARCHAR(32),
  `phone`         VARCHAR(20)      NOT NULL,
  `country_code`  CHAR(2)          NOT NULL,
  `timezone_name` VARCHAR(40)      NOT NULL,
  `created_at`    TIMESTAMP                 DEFAULT CURRENT_TIMESTAMP,
  `updated_at`    TIMESTAMP                 DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `idx_first_name` (`first_name`),
  KEY `idx_last_name` (`last_name`),
  KEY `idx_phone` (`phone`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

INSERT INTO `contacts` (`first_name`, `last_name`, `phone`, `country_code`, `timezone_name`) VALUES
  ('John', 'Doe', '+1-541-754-3010', 'US', 'America/Jamaica'),
  ('Quei', 'Lin', '+86 111 1234 5678', 'CN', 'Asia/Shanghai'),
  ('Matt', 'Bar', '+1 123 456 7890', 'US', 'America/Los_Angeles');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(64)      NOT NULL,
  `email`      VARCHAR(255)     NOT NULL,
  `created_at` TIMESTAMP                 DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP                 DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_email` (`email`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`) VALUES
  (1, 'John Doe', 'john.doe@example.com'),
  (2, 'Quei Lin', 'quei.lin@example.com'),
  (3, 'Matt Bar', 'mat.bar@example.com');

/**
 * OAuth
 */

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE `oauth_clients` (
  `id`         INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id`    INT(10) UNSIGNED NOT NULL,
  `name`       VARCHAR(255)     NOT NULL,
  `secret`     VARCHAR(100)     NOT NULL,
  `redirect`   VARCHAR(255)     NOT NULL,
  `revoked`    TINYINT(1)                DEFAULT 0,
  `created_at` TIMESTAMP                 DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP                 DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_id_secret` (`id`, `secret`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

INSERT INTO `oauth_clients` (`user_id`, `name`, `secret`, `redirect`) VALUES
  (1, 'VM', '5ePOwPFY7RTsjK4IA+EsI7gvZR/d49tXUjhdA', '#'),
  (2, 'VM', 'ZxW7mZLKIbI3MAsKu1uF+B39ls6ySa02FKo8a', '#'),
  (3, 'VM', 'gnQJjvDvo9C7Th3tQTG0GTgkmTHa664CBmJ3o', '#');

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE `oauth_access_tokens` (
  `id`         VARCHAR(100),
  `name`       VARCHAR(255) DEFAULT NULL,
  `user_id`    INT(10) UNSIGNED,
  `client_id`  INT(10) UNSIGNED NOT NULL,
  `scopes`     TEXT         DEFAULT NULL,
  `revoked`    TINYINT(1)   DEFAULT 0,
  `expires_at` DATETIME     DEFAULT NULL,
  `created_at` TIMESTAMP    DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP    DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

