#!/usr/bin/env bash

source "hostaway/vagrant/scripts/helpers.sh"

echo "Setup PHP"

# Install PHP
if [ -f /usr/bin/php7.2 ]; then
    echo "PHP already installed."
else
    echo "PHP is not installed. Installing..."

    # Add required repos
    add-apt-repository -y universe
    add-apt-repository -y ppa:ondrej/php
    add-apt-repository -y ppa:ondrej/nginx

    # Update packages after adding ppa
    apt-get-update

    apt-get-install-package php7.2
fi

# Install extensions
EXT=(curl zip mysql xml mbstring memcached)
for EXT_SUFFIX in "${EXT[@]}"
do
    if ls /etc/php/7.2/mods-available | grep -q ${EXT_SUFFIX}; then
        echo "PHP | ${EXT_SUFFIX} already installed"
    else
        echo "PHP | ${EXT_SUFFIX} is not installed. Installing..."
        apt-get-install-package php7.2-${EXT_SUFFIX}
    fi
done

# install FPM
if [ -f /usr/sbin/php-fpm7.2 ]; then
    echo "PHP | FPM already installed."
else
    echo "PHP | FPM is not installed. Installing..."

    # Add required repos
    apt-get-install-package php7.2-fpm

    # Restart FPM
    restart-service php7.2-fpm
fi

# Configure XDebug
if /usr/bin/php -v | grep -q Xdebug; then
    echo "PHP | Xdebug already installed"
else
    echo "PHP | Xdebug is not installed. Installing..."

    # Getting a package
    apt-get-install-package php-xdebug

    # Create log if not exists
    XDEBUG_LOG=/tmp/xdebug_remote.log
    touch ${XDEBUG_LOG}
    chmod a+rw ${XDEBUG_LOG}

    # Update config
    echo "
xdebug.remote_autostart = 1
xdebug.remote_enable = 1
xdebug.remote_handler = dbgp
xdebug.remote_log = ${XDEBUG_LOG}
xdebug.remote_mode = req
xdebug.remote_port = 9000
xdebug.remote_connect_back = 1
" | sudo tee -a /etc/php/7.2/mods-available/xdebug.ini

    # Restart FPM
    restart-service php7.2-fpm
fi
