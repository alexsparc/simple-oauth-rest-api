#!/usr/bin/env bash

function apt-get-update() {
    echo "Performing apt update"
    if [ `whoami` = "root" ]; then
        sudo apt-get -q update -y
    fi
}

function apt-get-upgrade() {
    echo "Performing apt upgrade"
    if [ `whoami` = "root" ]; then
        sudo apt-get -q upgrade -y
    fi
}

function apt-get-install-package() {
    echo "Installing ${1}"
    if [ `whoami` = "root" ]; then
        sudo apt-get -q install $1 -y
    fi
}

function apt-get-upgrade-package() {
    echo "Upgrading ${1}"
    if [ `whoami` = "root" ]; then
        sudo apt-get -q upgrade $1 -y
    fi
}

function restart-service() {
    echo "Restarting ${1}"
    if [ `whoami` = "root" ]; then
        sudo service ${1} restart
    fi
}
