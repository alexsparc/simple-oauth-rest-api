#!/usr/bin/env bash

source "hostaway/vagrant/scripts/helpers.sh"

echo "Setup Nginx"

if [ -f /usr/sbin/nginx ]; then
    echo "Nginx already installed."
    exit 0
fi

PROJECT_NAME=hostaway
SERVER_NAME=${PROJECT_NAME}.app

# Install latest nginx version from community maintained ppa
add-apt-repository ppa:nginx/stable

# Update packages after adding ppa
apt-get-update

# Install Nginx
apt-get-install-package nginx

# Create and save simple config
echo "server {
    listen 80;
    server_name ${SERVER_NAME};
    root \"/home/vagrant/${PROJECT_NAME}/public\";
    index index.html index.htm index.php;
    charset utf-8;
    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }
    access_log off;
    error_log  /var/log/nginx/${PROJECT_NAME}-error.log error;
    sendfile off;
    client_max_body_size 100m;
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
    }
    location ~ /\.ht {
        deny all;
    }
}
" > "/etc/nginx/sites-available/${SERVER_NAME}"

# Make new config available
ln -fs "/etc/nginx/sites-available/${SERVER_NAME}" "/etc/nginx/sites-enabled/${SERVER_NAME}"

# Disable default
rm -f "/etc/nginx/sites-enabled/default"

# Apply changes
restart-service nginx
