#!/usr/bin/env bash

KEYS_DIR=../.ssl

if [ ! -d ${KEYS_DIR} ]; then

    echo "Generating SSL keys"
    sudo usermod -a -G www-data vagrant
    mkdir -p ${KEYS_DIR}

    echo 'Generating a private key'
    openssl genrsa -passout pass:hostaway -out ${KEYS_DIR}/hostaway 2048
    sudo chown -R vagrant:www-data ${KEYS_DIR}/hostaway
    sudo chmod g+r,o-r ${KEYS_DIR}/hostaway

    echo 'Generating a public key'
    openssl rsa -in ${KEYS_DIR}/hostaway -passin pass:hostaway -pubout -out ${KEYS_DIR}/hostaway.pub
    sudo chown -R vagrant:www-data ${KEYS_DIR}/hostaway.pub
    sudo chmod g+r,o-r ${KEYS_DIR}/hostaway.pub
fi


echo "Update composer dependencies"
/usr/bin/composer -q update

echo "Initialize database structure"
/usr/bin/mysql < vagrant/database/init.sql

# Print readme
cat vagrant/notes/readme
