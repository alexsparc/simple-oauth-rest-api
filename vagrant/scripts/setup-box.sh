#!/usr/bin/env bash

source "hostaway/vagrant/scripts/helpers.sh"

echo "Setup box"

# Fetch all updates
apt-get-update && apt-get-update

# Get an add-apt-repository package
apt-get-install-package software-properties-common

# Install ifupdown
apt-get-install-package ifupdown

# Install memcached
if [ -f /usr/bin/memcached ]; then
    echo "Memcached already installed"
    apt-get-upgrade-package memcached
else
    echo "Memcached is not installed"
    apt-get-install-package memcached
fi

# Install composer
if [ -f /usr/bin/composer ]; then
    echo "Composer already installed"
    apt-get-upgrade-package composer
else
    echo "Composer is not installed"
    apt-get-install-package composer
fi

# Auto-switch directory on "vagrant ssh"
if cat /home/vagrant/.profile | grep -q "/home/vagrant/hostaway"; then
    echo "Auto-switch already installed"
else
    echo "Auto-switch is not installed"
    cat <<EOT >> "/home/vagrant/.profile"

# Auto-switch to project directory
cd "/home/vagrant/hostaway"

EOT
fi



