# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure('2') do |config|

  #
  # Box Setup
  #
  config.vm.box = 'ubuntu/bionic64'
  config.vm.provider 'virtualbox' do |p|
    p.memory = '4096'
  end
  config.vm.hostname = 'hostaway'
  config.vm.network 'private_network', ip: '192.168.50.10'
  config.vm.synced_folder '.', '/home/vagrant/hostaway'

  #
  # SSH
  #
  config.ssh.forward_agent = true
  config.ssh.insert_key = true

  id_rsa = File.expand_path('~/.ssh/id_rsa')
  id_rsa_pub = File.expand_path('~/.ssh/id_rsa.pub')

  if File.exist? id_rsa_pub and File.exist? id_rsa
    config.vm.provision 'shell' do |s|
      s.privileged = false
      s.inline = 'echo "$1" > /home/vagrant/.ssh/$2 && chmod 600 /home/vagrant/.ssh/$2'
      s.args = [File.read(id_rsa), id_rsa.split('/').last]
    end
    config.vm.provision 'shell' do |s|
      s.inline = 'echo $1 | grep -xq "$1" /home/vagrant/.ssh/authorized_keys || echo "\n$1" | tee -a /home/vagrant/.ssh/authorized_keys'
      s.args = [File.read(id_rsa_pub)]
    end
  else
    puts 'Please create a local SSH key with name "id_rsa"'
    exit
  end

  #
  # Provision
  #
  config.vm.provision 'shell', path: 'vagrant/scripts/setup-box.sh'
  config.vm.provision 'shell', path: 'vagrant/scripts/setup-nginx.sh'
  config.vm.provision 'shell', path: 'vagrant/scripts/setup-php.sh'
  config.vm.provision 'shell', path: 'vagrant/scripts/setup-mysql.sh'
  config.vm.provision 'shell', path: 'vagrant/scripts/setup-app.sh', privileged: false

end
